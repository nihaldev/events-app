import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import {AppRoutingModule} from './app-routing.module';


import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AddEventComponent } from './components/add-event/add-event.component';

import { ModalModule, BsDropdownModule, BsDatepickerModule } from 'ngx-bootstrap';
import {BsModalService, BsModalRef} from 'ngx-bootstrap';
import { DataShareService } from './services/data-share.service';
import { AuthGuard } from './auth-guard';
import { UserAuthComponent } from './components/user-auth/user-auth.component';
import { UserEventsComponent } from './components/user-events/user-events.component';
import { EventDetailComponent } from './components/event-detail/event-detail.component'

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DashboardComponent,
    AddEventComponent,
    UserAuthComponent,
    UserEventsComponent,
    EventDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    BsDatepickerModule.forRoot()
  ],
  entryComponents: [
    AddEventComponent
  ],
  providers: [
    DataShareService,
    AuthGuard,
    BsModalService,
    BsModalRef
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
