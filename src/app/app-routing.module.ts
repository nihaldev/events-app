import { NgModule }             from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { UserAuthComponent } from './components/user-auth/user-auth.component';
import { UserEventsComponent } from './components/user-events/user-events.component';
import { EventDetailComponent } from './components/event-detail/event-detail.component';
import { AuthGuard } from './auth-guard';

export const routes: Routes = [
  { path: '', redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  { path: 'dashboard',
    component: DashboardComponent,
    data: {
      title: 'Events Dashboard'
    },
    canActivate: [AuthGuard]
  },
  { path: 'signin',
    component: UserAuthComponent,
    data: {
      title: 'Sign In Into Events App'
    }
  },
  { path: 'my-events',
    component: UserEventsComponent,
    data: {
      title: 'User Event Listing'
    },
    canActivate: [AuthGuard]
  },
  { path: 'event-detail/:id',
    component: EventDetailComponent,
    data: {
      title: 'Event Details'
    },
    canActivate: [AuthGuard]
  }
  // { path: '**',
  //   component: PageNotFoundComponent
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: false})],
  exports: [RouterModule]
})

export class AppRoutingModule {}
