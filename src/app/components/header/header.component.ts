import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap';
import { AddEventComponent } from '../add-event/add-event.component'
import { DataShareService } from '../../services/data-share.service'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})

export class HeaderComponent implements OnInit {

  constructor(
  	public modalRef: BsModalRef,
    private modalService: BsModalService,
    private dataShareService: DataShareService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  /**
   * Open add event form modal
   */
  openCartModal() {
		let options = {
			class: 'modal-lg event-popup',
			animated: true,
			backdrop: true,
			ignoreBackdropClick: false
		};
		this.modalRef = this.modalService.show(AddEventComponent, options);
	}

  /**
   * Clear all localStorage values
   * Redirect to signin
   */
  logout(){
    localStorage.removeItem("events_data");
    localStorage.removeItem("user_data");
    this.router.navigate(['/signin']);
    this.dataShareService.initialiseEventData();
  }

  /**
   * route to user event tab
   */
  routeToMyEvents() {
    this.router.navigate(['/my-events']);
  }

}
