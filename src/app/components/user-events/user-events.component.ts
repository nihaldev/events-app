import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { takeWhile } from 'rxjs/operators';
import { DataShareService } from '../../services/data-share.service'
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { AddEventComponent } from '../add-event/add-event.component'

@Component({
  selector: 'app-user-events',
  templateUrl: './user-events.component.html',
  styleUrls: ['./user-events.component.less']
})
export class UserEventsComponent implements OnInit {

  userEvents: any;
  alive: boolean = true;
  user: any;

  constructor(
    private modalRef: BsModalRef,
    private modalService: BsModalService,
    private dataShareService: DataShareService,
    private router: Router
  ) { }

  ngOnInit() {
    this.user = this.dataShareService.getUserEmail();
    this.subscribeToEventUpdates();
    this.filterUserEvents();
  }

  /**
   * Filter out events created by user from all the events
   * @return void
   */
  filterUserEvents() {
    var totalEvents = this.dataShareService.getEventData();
    this.userEvents = totalEvents.filter(event => {
      return event.user === this.user;
    });
  }

  /**
   * Subsribe to changes in {Observable<any>} notifyEventUpdate
   * @return void
   */
  subscribeToEventUpdates(){
    this.dataShareService.subscribeToEventChange()
    .pipe(takeWhile(() => this.alive))
    .subscribe(data => {
      if(data){
        this.filterUserEvents();
      }
    });
  }

  /**
   * Eid an event created bu user
   * @param {Object} eventData
   */
  editEvent(eventData: any) {
		let options = {
			class: 'modal-lg event-popup',
			animated: true,
			backdrop: true,
			ignoreBackdropClick: false,
      data: eventData
		};
		this.modalRef = this.modalService.show(AddEventComponent, options);
	}

  /**
   * Delete an event created bu user
   * @param {Object} event
   */
  removeEvent(event: any) {
    this.dataShareService.deleteEvent(event);
  }

  /**
   * Show alert dialog to confirm event remove
   * @return void
   */
  showDeleteAlert(event: any) {
    var alert = confirm("Are you sure? Data will be lost!");
    if (alert == true) {
        this.removeEvent(event);
    }
  }

  /**
   * Route to event detail page
   * @param {number} id
   */
  routeToEventDetail(id: number) {
    this.router.navigate(['/event-detail', id]);
  }


}
