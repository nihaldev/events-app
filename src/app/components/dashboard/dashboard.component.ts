import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataShareService } from '../../services/data-share.service'
import { takeWhile } from 'rxjs/operators';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less']
})
export class DashboardComponent implements OnInit {

  constructor(
    private dataShareService: DataShareService,
    private router: Router
  ) { }

  events: any;
  alive: boolean = true;

  ngOnInit() {
    this.subscribeToEventUpdates();
    this.events = this.dataShareService.getEventData();
  }

  /**
   * Subsribe to changes in {Observable<any>} notifyEventUpdate
   * @return void
   */
  subscribeToEventUpdates(){
    this.dataShareService.subscribeToEventChange()
    .pipe(takeWhile(() => this.alive))
    .subscribe(data => {
      if(data){
        this.events = this.dataShareService.getEventData();
      }
    });
  }

  /**
   * Route to event detail page
   */
  routeToEventDetail(id: number) {
    this.router.navigate(['/event-detail', id]);
  }

  /**
   * Set {boolean} alive to flase to prevent memory leakage
   */
  ngOnDestroy() {
    this.alive = false;
  }

}
