import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataShareService } from '../../services/data-share.service'

@Component({
  selector: 'app-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.less']
})
export class EventDetailComponent implements OnInit {

  constructor(
    private dataShareService: DataShareService,
    private router: ActivatedRoute
  ) { }

  id: string;
  user: any
  eventDetail: any;
  isAlreadyJoined: boolean = false;

  ngOnInit() {
    this.user = this.dataShareService.getUserEmail();
    this.router.params.subscribe(params => {
			this.id = params.id;
      this.getEventDetail();
    })
  }

  /**
   * Filter out events created by user from all the events
   * @return void
   */
  getEventDetail() {
    var totalEvents = this.dataShareService.getEventData();
    let events = totalEvents.filter(event => {
      return event.id == this.id;
    });
    this.eventDetail = events[0];

    //find if already joined
    this.eventDetail.members.forEach((value) => {
      if(value == this.user){
        this.isAlreadyJoined = true;
      }
    });
  }

  /**
   * Join/Leave an event
   * @return void
   */
   joinEvent() {
     this.isAlreadyJoined = !this.isAlreadyJoined;
     var isExisting;
      this.eventDetail.members.forEach((value, index) => {
        if(value == this.user){
          isExisting = index;
        }
      });
      if(isExisting != null){
        this.eventDetail.members.splice(isExisting, 1);
      } else {
       this.eventDetail.members.push(this.user);
      }
     this.dataShareService.editEventData(this.eventDetail);
   }
}
