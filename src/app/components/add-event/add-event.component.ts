import { Component, OnInit } from '@angular/core';
import { DataShareService } from '../../services/data-share.service'
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-add-event',
  templateUrl: './add-event.component.html',
  styleUrls: ['./add-event.component.less']
})

export class AddEventComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private dataShareService: DataShareService,
    private modalService: BsModalService,
    public modalRef: BsModalRef
  ) { }

  eventForm: FormGroup;
  existingEvents: any = [];

  ngOnInit() {
    console.log('modalService', this.modalService);
    this.existingEvents = this.dataShareService.getEventData();
    this.eventForm = this.formBuilder.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      duration: new FormControl(new Date()),
      location: ['', Validators.required],
      fees: ['', Validators.required],
      tags: [''],
      participants: [''],
      image: ['']
    });
    if(this.modalService.config['data']){
      this.fillEventsForm(this.modalService.config['data']);
    }
  }

  /**
   * Format form data and save to localStorage
   */
  addEvent() {
    let event = {
      "title": this.eventForm.value.title,
      "description": this.eventForm.value.description,
      "duration":  this.setEventDuaration(this.eventForm.value.duration),
      "location": this.eventForm.value.location,
      "fees": this.eventForm.value.fees,
      "tags": this.eventForm.value.tags,
      "participants": this.eventForm.value.participants,
      "image": this.eventForm.value.image,
      "id": this.modalService.config['data'] ? this.modalService.config['data'].id : this.existingEvents[this.existingEvents.length-1].id + 1,
      "user": this.dataShareService.getUserEmail(),
      "members": []
    }
    if(this.modalService.config['data']){
      this.dataShareService.editEventData(event);
    } else {
      this.dataShareService.setEventData(event);
    }
    this.modalRef.hide();
  }

  /**
   * Check if add/edit form and format data
   */
  setEventDuaration(duration: any) {
    if(Array.isArray(duration)){
      return this.getFormattedDate(duration[0]) +" - "+ this.getFormattedDate(duration[1]);
    }else{
      return duration;
    }
  }

  /**
   * Format datepicker object
   * @param {Object} date
   * @returns {string}
   */
  getFormattedDate(date) {
    let year = date.getFullYear();

    let month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;

    let day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;

    return month + '/' + day + '/' + year;
  }

  /**
   * Prefill form field values
   * @param {Object} userAddress
   */
  fillEventsForm(userAddress: any) {
    Object.keys(userAddress).forEach(k=>{
      let control = this.eventForm.get(k);
      if(control && (k != 'id' && k != 'user' && k != 'members'))
      control.setValue(userAddress[k],{onlySelf:true});
    });
  }

  /**
   * Close the form modal
   */
  closePopup() {
    this.modalRef.hide();
  }

}
