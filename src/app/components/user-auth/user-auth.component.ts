import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-auth',
  templateUrl: './user-auth.component.html',
  styleUrls: ['./user-auth.component.less']
})
export class UserAuthComponent implements OnInit {

  signinForm: FormGroup;
  registerForm: FormGroup;
  alreadyRegistered: boolean = false;
  pwdMismatch: boolean;
  dataMismatch: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router
  ) { }

  ngOnInit() {
    this.createRegisterForm();
    this.createSignInForm();
  }

  /**
   * Create user sign in form.
   * @return void
  */
  createSignInForm() {
    this.alreadyRegistered = !this.alreadyRegistered
    this.signinForm = this.formBuilder.group({
      name: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  /**
   * Create user sign up form.
   * @return void
  */
  createRegisterForm() {
    this.registerForm = this.formBuilder.group({
      name: new FormControl('', Validators.required),
      email: new FormControl('', {
        validators: [
          Validators.required,
          Validators.pattern('[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}')
        ], updateOn: 'blur'
      }),
      password: new FormControl('', {
        validators: [
          Validators.required,
          Validators.pattern('(?=.*?[0-9])[A-Za-z0-9!@#$%^&*]{6,}'),
        ]
      }),
      reEnterPassword: new FormControl('', Validators.required)
    });
  }

  /**
   * User registeration: Validation and routing
   * @return void
   */
  createUser() {
    if(this.registerForm.valid) {
      if(this.registerForm.value.password != this.registerForm.value.reEnterPassword) {
        this.pwdMismatch = true;
      }
      else {
        this.pwdMismatch = false;
        let userData = {
          "name": this.registerForm.value.name,
          "email": this.registerForm.value.email,
          "password": this.registerForm.value.password
        }
        localStorage.setItem("user_data", JSON.stringify(userData));
        this.router.navigate(['/dashboard']);
      }
    }
  }

  /**
   * Sign in an existing user: Validation and routing
   * @return void
   */
  signInUser() {
    var user = JSON.parse(localStorage.getItem('user_data'));
    if(this.signinForm.valid) {
      if(this.signinForm.value.name != user.name || this.signinForm.value.password != user.password) {
        this.dataMismatch = true;
      }
      else {
        this.dataMismatch = false;
        let userData = {
          "name": this.registerForm.value.name,
          "email": this.registerForm.value.email,
          "password": this.registerForm.value.password
        }
        localStorage.setItem("user_data", JSON.stringify(userData));
        this.router.navigate(['/dashboard']);
      }
    }
  }


}
