import { Injectable } from '@angular/core';
import { BehaviorSubject } from "rxjs";
import { map } from 'rxjs/operators';

var EVENTS = [
  {
    id: 1,
    title: 'Mock Event 1',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
    duration: '10/11/2018 - 16/11/2018',
    location: 'Chennai',
    fees: '8000',
    tags: '',
    participants: 100,
    image: 'https://michellesdesignhouse.com/wp-content/uploads/2017/11/event-production-company-p2-entertainment-group-1.jpg',
    user: 'default@events.com',
    members: []
  },
  {
    id: 2,
    title: 'Mock Event 2',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
    duration: '10/11/2018 - 16/11/2018',
    location: 'Calicut',
    fees: '25000',
    tags: '',
    participants: 30,
    image: 'https://mk0lightsfestp7owe1j.kinstacdn.com/wp-content/uploads/2017/01/San-Antonio_TX_2016.jpg',
    user: 'default@events.com',
    members: []
  },
  {
    id: 3,
    title: 'Mock Event 3',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
    duration: '10/11/2018 - 16/11/2018',
    location: 'Pune',
    fees: '1000',
    tags: '',
    participants: 25,
    image: 'http://farm4.static.flickr.com/3637/3638890517_7161f4c331.jpg',
    user: 'default@events.com',
    members: []
  },
  {
    id: 4,
    title: 'Mock Event 4',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
    duration: '10/11/2018 - 16/11/2018',
    location: 'Mumbai',
    fees: '5000',
    tags: '',
    participants: 40,
    image: 'https://tedconfblog.files.wordpress.com/2014/12/15447807795_55bb873910_k.jpg?w=900&h=598',
    user: 'default@events.com',
    members: []
  },
];

@Injectable({
  providedIn: 'root'
})

export class DataShareService {

  private notifyEventUpdate = new BehaviorSubject(null);
  private eventsData: any = [];

  constructor() {
    this.initialiseEventData();
  }

  /**
   * Add an event created by the user
   * @param {Object} event
   */
  setEventData(event){
    this.eventsData.push(event);
    this.updateAndNotifyEventChange();
  }

  /**
   * Edit an existing user event
   * @param {Object} event
   */
  editEventData(event: any){
    this.eventsData.forEach((item, i) => {
       if (item.id == event.id) {
         this.eventsData[i] = event;
       }
     });
    this.updateAndNotifyEventChange();
  }

  /**
   * Delete an existing user event
   * @param {Object} event
   */
  deleteEvent(event: any) {
    this.eventsData.forEach((item, i) => {
       if (item.id == event.id) {
        this.eventsData.splice(i, 1);
       }
     });
    this.updateAndNotifyEventChange();
  }

  /**
   * Update local storage and set value in Observable
   * @return void
   */
  updateAndNotifyEventChange(){
    localStorage.setItem("events_data", JSON.stringify(this.eventsData));
    this.notifyEventChange();
  }

  /** Method to fetch all events data
  * @returns {Object} eventsData
  */
  getEventData(){
    return this.eventsData;
  }

  /** subscribe to event CRUD operations in components
  * @returns {Observable<any>} notifyEventUpdate.
  */
  subscribeToEventChange(){
    return this.notifyEventUpdate.asObservable();
  }

  /** notify event CRUD operations from components to service
  * @return void
  */
  notifyEventChange(){
    this.notifyEventUpdate.next(true);
  }

  /** initialise all events data
  * @return void
  */
  initialiseEventData(){
    if(localStorage.getItem('events_data') === null) {
      localStorage.setItem('events_data', JSON.stringify(EVENTS));
      this.eventsData = EVENTS;
    } else {
      this.eventsData = JSON.parse(localStorage.getItem('events_data'));
    }
  }

  /**
   * Track purchased products
   * @returns {String} user email address.
   */
  getUserEmail() {
    return JSON.parse(localStorage.getItem('user_data')).email;
  }

}
