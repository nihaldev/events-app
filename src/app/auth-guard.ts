import { Injectable } from '@angular/core';
import { CanActivate, Router, RouterStateSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements CanActivate {

  constructor(
    private router: Router
  ) { }

  canActivate() {
      if (localStorage.getItem('user_data')) {
        return true;
      }
      this.router.navigate(['/signin']);
      return false;
  }
}
